

readonly image_x_size=224
readonly image_y_size=224
readonly in_channels=64

readonly kernel_x_size=3
readonly kernel_y_size=3
readonly out_channels=64


readonly output_x_start_pos=0
readonly output_y_start_pos=0

# readonly all_tile_size_x=(5 6)
# readonly all_tile_size_y=(5 6)
# readonly in_channels_per_thread=(128 64)


readonly all_tile_size_x=(3 4 5 6)
readonly all_tile_size_y=(3 4 5 6)
readonly in_channels_per_thread=(64 32 16 8)
