#!/usr/bin/env bash

set -e
set -o pipefail

# Activate debug
# set -x


# if you only want performance data, send err to dev null
# ./run.sh 2>/dev/null

./exec_all_configs.sh ../config.sh

