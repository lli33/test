#!/usr/bin/env bash

set -e
set -o pipefail

# Activate debug
# set -x


config_file=$1

source $config_file


echo "tile_size_x, tile_size_y, in_channel_per_share, func_name, cpu_time_ms, gpu_time_ms, diff_percentage"

for itx in "${all_tile_size_x[@]}" ; do
	for ity in "${all_tile_size_y[@]}" ; do
		for iicpt in "${in_channels_per_thread[@]}" ; do
			if [ -d ./all_configs/code_${itx}_${ity}_${iicpt} ]; then
				>&2 echo "./exec_a_config.sh ./all_configs/code_${itx}_${ity}_${iicpt}/"
				./exec_a_config.sh ./all_configs/code_${itx}_${ity}_${iicpt}/
			fi
		done
	done
done

# ./exec_a_config.sh ./all_configs/code_5_5_64
