#include <bits/stdc++.h>

using namespace std;

#include <kernel_constants.h>

#include <libconv.cpp>



int main(int argc, char *argv[])
{

	assert(argc == 2);
	const std::string kernel_path(argv[1]);

	constexpr int input_channels = Input_channels;
	constexpr int input_height = Original_input_height;
	constexpr int input_width = Original_input_width;
	constexpr int padded_input_height = Padded_Input_height_for_correctness;
	constexpr int padded_input_width = Padded_Input_width;
	constexpr int kernel_height = Kernel_height;
	constexpr int kernel_width = Kernel_width;
	constexpr int num_kernels = Num_kernels;
	constexpr int output_height = Output_height;
	constexpr int output_width = Output_width;

	constexpr int kernel_stride_height = 1;
	constexpr int kernel_stride_width = 1;

	constexpr int input_length = input_channels * input_height * input_width;
	constexpr int padded_input_length = input_channels * padded_input_height * padded_input_width;
	constexpr int kernel_length = num_kernels * input_channels * kernel_height * kernel_width ;
	constexpr int tmp_output_length = num_kernels * (padded_input_height - kernel_height + 1) * (padded_input_width - kernel_width + 1) ; 
	constexpr int output_length = num_kernels * output_height * output_width ;

	vector<float> input(padded_input_length,1.32f), kernel(kernel_length,2.38f);


	float * output = nullptr;


	lift_init(kernel_path);
	lift::execute(input.data(), kernel.data(), padded_input_length, kernel_length, tmp_output_length);



	return 0;
}
