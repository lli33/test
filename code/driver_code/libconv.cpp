
#include <bits/stdc++.h>

using namespace std;


    

#include <iostream>
#include <CL/cl2.hpp>
#include <fstream>

std::string readFile(const char *filename){

  std::ifstream in(filename, std::ios::in);

  if (in.fail())
  {
  std::cerr << "Error reading file " << filename << std::endl;
  exit(1); }

  std::string contents;
  in.seekg(0, std::ios::end);
  contents.resize(in.tellg());
  in.seekg(0, std::ios::beg);
  in.read(&contents[0], contents.size());
  in.close();
  return contents;
  }

    

int platformId = 0;
int deviceId = 0;

 std::vector<cl::Platform> allPlatforms;
 cl::Platform platform;
 std::vector<cl::Device> allDevices;
 cl::Device device;
 cl::Context context;
 cl::CommandQueue lift_queue;

 size_t lift_global_0 = 1, lift_global_1 = 1, lift_global_2 =1;

      ; 
std::string kernel_string_431;
cl::Program::Sources kernel_source_431;
cl::Program kernel_program_431;
cl::Kernel kernel_431;
cl::Kernel kernel_final_reduction;
cl::Kernel kernel_padding;
cl::Kernel kernel_padding1;
cl::Kernel kernel_padding2;
cl::Kernel kernel_padding3;
cl::Kernel kernel_last_row;
std::chrono::milliseconds cpu_clock_start_431;
std::chrono::milliseconds cpu_clock_end_431;
std::chrono::milliseconds cpu_clock_start_padding;
std::chrono::milliseconds cpu_clock_end_padding;
cl::Event event_431;
cl::Event event_final_reduction;
cl::Event event_padding;
cl::Event event_padding1;
cl::Event event_padding2;
cl::Event event_padding3;
cl::Event event_last_row;
; 
; 
; 
; 
; 
; 
; 
; 
; 
; 
; 
; 
void lift_init(const std::string & pwd){
    
	cl::Platform::get(&allPlatforms);
 if (allPlatforms.size() == 0) {
 std::cerr << " No platforms found. Check OpenCL installation!" << std::endl;
 exit(1);
 }

 platform = allPlatforms[platformId];

 platform.getDevices(CL_DEVICE_TYPE_ALL, &allDevices);
 if (allDevices.size() == 0) {
 std::cerr << " No devices found. Check OpenCL installation!" << std::endl;
 exit(1);
 }

 device = allDevices[deviceId];

 std::cerr << "Using platform: " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;
 std::cerr << "Using device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;

 // create context
 cl::Context tmp_context({ device });
 context = std::move(tmp_context);

 // create queue
 cl::CommandQueue tmp_queue(context, device, CL_QUEUE_PROFILING_ENABLE);
 lift_queue = std::move(tmp_queue);
      ; 
    kernel_string_431 = readFile((pwd + "/kernel_431.cl").c_str()); 
    kernel_source_431 = cl::Program::Sources(1, {kernel_string_431.c_str(), kernel_string_431.length()}); 
    kernel_program_431 = cl::Program(context, kernel_source_431); 


    //not too much use
    /* const char *options = "-fkernel-vectorizer=4 -fkernel-unroller=2"; */
    /* if ((kernel_program_431.build({ device }, options, NULL, NULL) != CL_SUCCESS)){ */

    if ((kernel_program_431.build({ device }) != CL_SUCCESS)){
        std::cerr << "kernel build error" << std::endl; 
        char* log; 
        size_t logsize; 
        assert((clGetProgramBuildInfo(kernel_program_431.get(), device.get(), CL_PROGRAM_BUILD_LOG, 0, NULL, &logsize) == CL_SUCCESS)); 
        log = (char*)malloc(sizeof(char) * logsize); 
        assert((clGetProgramBuildInfo(kernel_program_431.get(), device.get(), CL_PROGRAM_BUILD_LOG, logsize, log, NULL) == CL_SUCCESS)); 
        std::cout << log << std::endl; 
        exit(1); 
    }
    kernel_431 = cl::Kernel(kernel_program_431, "KERNEL"); 
    kernel_final_reduction = cl::Kernel(kernel_program_431, "REDUCTION"); 
    kernel_padding = cl::Kernel(kernel_program_431, "PADDING"); 
    kernel_padding1 = cl::Kernel(kernel_program_431, "PADDING_1"); 
    kernel_padding2 = cl::Kernel(kernel_program_431, "PADDING_2"); 
    kernel_padding3 = cl::Kernel(kernel_program_431, "PADDING_3"); 
    kernel_last_row = cl::Kernel(kernel_program_431, "LAST_ROW_KERNEL"); 

}

double cpu_time_in_ms( std::chrono::milliseconds start, std::chrono::milliseconds finish ){
 return (finish - start).count();
}

double gpu_time_in_ms( cl::Event event ){

  cl_ulong start;
  cl_ulong end;
  cl_int err;

  event.wait();

  err = clGetEventProfilingInfo(event(), CL_PROFILING_COMMAND_START,
                                sizeof(start), &start, NULL);
  assert(err == CL_SUCCESS);

  err = clGetEventProfilingInfo(event(), CL_PROFILING_COMMAND_END,
                                sizeof(end), &end, NULL);
  assert(err == CL_SUCCESS);

  return ((double)(end - start)) * 1.0e-6;
}

double diff_percent(double lhs, double rhs) {
  if(std::min(lhs,rhs)==0)
    return -1;
  else
    return std::abs(lhs-rhs)/std::min(lhs,rhs);
}

          ; 
void print_clock(){
    /* std::cout<<"tile_size_x, tile_size_y, in_channel_per_share, func_name, cpu_time_ms, gpu_time_ms, diff_percentage"<<std::endl; */ 
    {
        double cpu_time_ms = cpu_time_in_ms(cpu_clock_start_431, cpu_clock_end_431);
        double gpu_time_ms = gpu_time_in_ms(event_431);
        double diff_pc = diff_percent(cpu_time_ms, gpu_time_ms);
        std::cout<<Tile_size_x<< "," << Tile_size_y << "," << In_channel_per_share << "," <<"OclFunCall_431"<<", "<<cpu_time_ms<<", "<<gpu_time_ms<<", "<<diff_pc<<std::endl; 

	/* std::cout << "total_time_cpu = " << cpu_time_ms << std::endl; */
	/* std::cout << "total_time_gpu = " << gpu_time_ms << std::endl; */
	
    }
}
void post_execute(){
    print_clock(); 
}

namespace lift {; 

void execute(float * v_initial_param_419_63, float * v_initial_param_420_64, const int padded_input_size , const int kernel_length, const int tmp_output_length ){

    //cl::Buffer v_user_func_426_66(context, CL_MEM_READ_ONLY , (input_size * sizeof(float)));
    cl::Buffer padded_input(context, CL_MEM_READ_ONLY , (padded_input_size * sizeof(float)));
    cl::Buffer v_user_func_428_67(context, CL_MEM_READ_ONLY , (kernel_length * sizeof(float)));

    cl::Buffer v_user_func_431_69(context, CL_MEM_WRITE_ONLY , (KERNEL_ITER_OUTPUT_FACTOR*tmp_output_length * sizeof(float)));
    //cl::Buffer final_output(context, CL_MEM_WRITE_ONLY , (1605632 * sizeof(float)));
    //v_user_func_432_70 = reinterpret_cast<float *>(malloc((1605632 * sizeof(float)))); 
    

    lift_queue.enqueueWriteBuffer(padded_input, CL_TRUE, 0, (padded_input_size * sizeof(float)), v_initial_param_419_63, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    ; 
    lift_queue.enqueueWriteBuffer(v_user_func_428_67, CL_TRUE, 0, (kernel_length * sizeof(float)), v_initial_param_420_64, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS);  
    ; 
    ; 

    kernel_431.setArg(0, padded_input); 
    kernel_431.setArg(1, v_user_func_428_67); 
    kernel_431.setArg(2, v_user_func_431_69); 



    ;
    lift_queue.enqueueNDRangeKernel(kernel_431, cl::NullRange, cl::NDRange(KERNEL_DIM0,KERNEL_DIM1,KERNEL_DIM2), cl::NullRange, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 

    


    cpu_clock_start_431 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 

    
    lift_queue.enqueueNDRangeKernel(kernel_431, cl::NullRange, cl::NDRange(KERNEL_DIM0,KERNEL_DIM1,KERNEL_DIM2), cl::NullRange, NULL, (&event_431)); 
    assert(lift_queue.finish() == CL_SUCCESS); 


    cpu_clock_end_431 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 

    
    
    post_execute(); 

}
}; 
