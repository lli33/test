

//data layout, from outputmost to innermost
// image: #in_channels x height x width
// kernel: #out_channels x in_channels x height x width
// output: #out_channels x output_height x output_width

#define Input_channels 64
#define Original_input_width 224
#define Original_input_height 224
#define Padded_Input_width 228
#define Padded_Input_height_for_main_kernel 224
#define Padded_Input_height_for_correctness 226
#define Kernel_width 3
#define Kernel_height 3
#define Num_kernels 64

#define Output_width 226
#define Output_height 224
#define Output_width_for_correctness 224
#define Output_height_for_correctness 224

#define Tile_size_x 4
#define Tile_size_y 4
#define In_channel_per_share 8


//-------------------------------
	
// dim0 to dim2: (904, 112, 64)


#define KERNEL_DIM0 904
#define KERNEL_DIM1 112
#define KERNEL_DIM2 64

#define KERNEL_ITER_OUTPUT_FACTOR 8




kernel void KERNEL(const global float * restrict input,
			      const global float * restrict weights,
			      global float * restrict output
			      ) {


const size_t kx = get_global_id(2);
const size_t wy = get_global_id(1);
size_t wx = get_global_id(0);


size_t const which_portion = wx / 113 ;
wx = wx % 113 ;
float o0_0 = 0.0f;
float o0_1 = 0.0f;
float o1_0 = 0.0f;
float o1_1 = 0.0f;
size_t const output_offset0 = kx * Output_height * Output_width + (2  * wy + 0 ) * Output_width + 2 * wx + which_portion * Num_kernels * Output_height * Output_width + 0 + 0 * Output_width;
size_t const output_offset1 = kx * Output_height * Output_width + (2  * wy + 1 ) * Output_width + 2 * wx + which_portion * Num_kernels * Output_height * Output_width + 0 + 0 * Output_width;
for(int icx = 0 + which_portion * 8 ; icx < (which_portion+1) * 8; ++icx){
    size_t const input_offset = icx * Padded_Input_height_for_correctness * Padded_Input_width + (2 * wy) * Padded_Input_width + 2 * wx + 0 + 0 * Padded_Input_width; 
    size_t const weight_offset = kx * Input_channels * Kernel_height * Kernel_width + icx * Kernel_height * Kernel_width ;

    float4 const wv0 = vload4(0, weights + weight_offset);
    float4 const wv1 = vload4(1, weights + weight_offset);
    float const w8 = weights[weight_offset + 8];


    float4 const v0_0=vload4( 0 , input + input_offset + 0 * Padded_Input_width + 0 * 4 );
    float4 const v1_0=vload4( 0 , input + input_offset + 1 * Padded_Input_width + 0 * 4 );
    float4 const v2_0=vload4( 0 , input + input_offset + 2 * Padded_Input_width + 0 * 4 );
    float4 const v3_0=vload4( 0 , input + input_offset + 3 * Padded_Input_width + 0 * 4 );

    o0_0 += wv0.s0 * v0_0.s0;
    o0_0 += wv0.s1 * v0_0.s1;
    o0_0 += wv0.s2 * v0_0.s2;
    o0_0 += wv0.s3 * v1_0.s0;
    o0_0 += wv1.s0 * v1_0.s1;
    o0_0 += wv1.s1 * v1_0.s2;
    o0_0 += wv1.s2 * v2_0.s0;
    o0_0 += wv1.s3 * v2_0.s1;
    o0_0 += w8 * v2_0.s2;
    o0_1 += wv0.s0 * v0_0.s1;
    o0_1 += wv0.s1 * v0_0.s2;
    o0_1 += wv0.s2 * v0_0.s3;
    o0_1 += wv0.s3 * v1_0.s1;
    o0_1 += wv1.s0 * v1_0.s2;
    o0_1 += wv1.s1 * v1_0.s3;
    o0_1 += wv1.s2 * v2_0.s1;
    o0_1 += wv1.s3 * v2_0.s2;
    o0_1 += w8 * v2_0.s3;

    o1_0 += wv0.s0 * v1_0.s0;
    o1_0 += wv0.s1 * v1_0.s1;
    o1_0 += wv0.s2 * v1_0.s2;
    o1_0 += wv0.s3 * v2_0.s0;
    o1_0 += wv1.s0 * v2_0.s1;
    o1_0 += wv1.s1 * v2_0.s2;
    o1_0 += wv1.s2 * v3_0.s0;
    o1_0 += wv1.s3 * v3_0.s1;
    o1_0 += w8 * v3_0.s2;
    o1_1 += wv0.s0 * v1_0.s1;
    o1_1 += wv0.s1 * v1_0.s2;
    o1_1 += wv0.s2 * v1_0.s3;
    o1_1 += wv0.s3 * v2_0.s1;
    o1_1 += wv1.s0 * v2_0.s2;
    o1_1 += wv1.s1 * v2_0.s3;
    o1_1 += wv1.s2 * v3_0.s1;
    o1_1 += wv1.s3 * v3_0.s2;
    o1_1 += w8 * v3_0.s3;

}
vstore2( (float2) (o0_0, o0_1) , 0, output + output_offset0 + 4 * 0);
vstore2( (float2) (o1_0, o1_1) , 0, output + output_offset1 + 4 * 0);
} 

