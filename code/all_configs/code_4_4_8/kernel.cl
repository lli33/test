

kernel void KERNEL(const global float * restrict input,
			      const global float * restrict weights,
			      global float * restrict output
			      ) {


const size_t kx = get_global_id(2);
const size_t wy = get_global_id(1);
size_t wx = get_global_id(0);


size_t const which_portion = wx / 113 ;
wx = wx % 113 ;
float o0_0 = 0.0f;
float o0_1 = 0.0f;
float o1_0 = 0.0f;
float o1_1 = 0.0f;
size_t const output_offset0 = kx * Output_height * Output_width + (2  * wy + 0 ) * Output_width + 2 * wx + which_portion * Num_kernels * Output_height * Output_width + 0 + 0 * Output_width;
size_t const output_offset1 = kx * Output_height * Output_width + (2  * wy + 1 ) * Output_width + 2 * wx + which_portion * Num_kernels * Output_height * Output_width + 0 + 0 * Output_width;
for(int icx = 0 + which_portion * 8 ; icx < (which_portion+1) * 8; ++icx){
    size_t const input_offset = icx * Padded_Input_height_for_correctness * Padded_Input_width + (2 * wy) * Padded_Input_width + 2 * wx + 0 + 0 * Padded_Input_width; 
    size_t const weight_offset = kx * Input_channels * Kernel_height * Kernel_width + icx * Kernel_height * Kernel_width ;

    float4 const wv0 = vload4(0, weights + weight_offset);
    float4 const wv1 = vload4(1, weights + weight_offset);
    float const w8 = weights[weight_offset + 8];


    float4 const v0_0=vload4( 0 , input + input_offset + 0 * Padded_Input_width + 0 * 4 );
    float4 const v1_0=vload4( 0 , input + input_offset + 1 * Padded_Input_width + 0 * 4 );
    float4 const v2_0=vload4( 0 , input + input_offset + 2 * Padded_Input_width + 0 * 4 );
    float4 const v3_0=vload4( 0 , input + input_offset + 3 * Padded_Input_width + 0 * 4 );

    o0_0 += wv0.s0 * v0_0.s0;
    o0_0 += wv0.s1 * v0_0.s1;
    o0_0 += wv0.s2 * v0_0.s2;
    o0_0 += wv0.s3 * v1_0.s0;
    o0_0 += wv1.s0 * v1_0.s1;
    o0_0 += wv1.s1 * v1_0.s2;
    o0_0 += wv1.s2 * v2_0.s0;
    o0_0 += wv1.s3 * v2_0.s1;
    o0_0 += w8 * v2_0.s2;
    o0_1 += wv0.s0 * v0_0.s1;
    o0_1 += wv0.s1 * v0_0.s2;
    o0_1 += wv0.s2 * v0_0.s3;
    o0_1 += wv0.s3 * v1_0.s1;
    o0_1 += wv1.s0 * v1_0.s2;
    o0_1 += wv1.s1 * v1_0.s3;
    o0_1 += wv1.s2 * v2_0.s1;
    o0_1 += wv1.s3 * v2_0.s2;
    o0_1 += w8 * v2_0.s3;

    o1_0 += wv0.s0 * v1_0.s0;
    o1_0 += wv0.s1 * v1_0.s1;
    o1_0 += wv0.s2 * v1_0.s2;
    o1_0 += wv0.s3 * v2_0.s0;
    o1_0 += wv1.s0 * v2_0.s1;
    o1_0 += wv1.s1 * v2_0.s2;
    o1_0 += wv1.s2 * v3_0.s0;
    o1_0 += wv1.s3 * v3_0.s1;
    o1_0 += w8 * v3_0.s2;
    o1_1 += wv0.s0 * v1_0.s1;
    o1_1 += wv0.s1 * v1_0.s2;
    o1_1 += wv0.s2 * v1_0.s3;
    o1_1 += wv0.s3 * v2_0.s1;
    o1_1 += wv1.s0 * v2_0.s2;
    o1_1 += wv1.s1 * v2_0.s3;
    o1_1 += wv1.s2 * v3_0.s1;
    o1_1 += wv1.s3 * v3_0.s2;
    o1_1 += w8 * v3_0.s3;

}
vstore2( (float2) (o0_0, o0_1) , 0, output + output_offset0 + 4 * 0);
vstore2( (float2) (o1_0, o1_1) , 0, output + output_offset1 + 4 * 0);
} 

