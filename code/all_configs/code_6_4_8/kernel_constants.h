

//data layout, from outputmost to innermost
// image: #in_channels x height x width
// kernel: #out_channels x in_channels x height x width
// output: #out_channels x output_height x output_width

#define Input_channels 64
#define Original_input_width 224
#define Original_input_height 224
#define Padded_Input_width 230
#define Padded_Input_height_for_main_kernel 224
#define Padded_Input_height_for_correctness 226
#define Kernel_width 3
#define Kernel_height 3
#define Num_kernels 64

#define Output_width 228
#define Output_height 224
#define Output_width_for_correctness 224
#define Output_height_for_correctness 224

#define Tile_size_x 6
#define Tile_size_y 4
#define In_channel_per_share 8


//-------------------------------
	
// dim0 to dim2: (456, 112, 64)


#define KERNEL_DIM0 456
#define KERNEL_DIM1 112
#define KERNEL_DIM2 64

#define KERNEL_ITER_OUTPUT_FACTOR 8


