

//data layout, from outputmost to innermost
// image: #in_channels x height x width
// kernel: #out_channels x in_channels x height x width
// output: #out_channels x output_height x output_width
  
  
  

void golden_convolution(float const *input,
		float const *weights,
		float *output,
		int const input_channels,
		int const input_height,
		int const input_width,
		int const kernel_height,
		int const kernel_width,
		int const num_kernels) {


	int const output_height = (input_height + 2) - kernel_height + 1;
	int const output_width = (input_width + 2) - kernel_width + 1;
	int x,y, input_idx, weight_idx, input_offset, weight_offset;

	//pad input with a frame of 0s
	int const padded_input_length = input_channels * (input_height + 2 ) * (input_width + 2);
	vector<float> padded_input( padded_input_length, 0.0f);
	for (int i = 0; i < input_channels; ++i) {

		for (int r = 0; r < input_height; ++r) {
			for (int c = 0; c < input_width; ++c) {
				padded_input[ i * (input_width +2) * (input_height +2) + (r+1)*(input_width+2) + c+1] =
					input[ i * input_width * input_height + r*input_width + c ];
			}

		}
	}


	for(int kx = 0; kx < num_kernels; ++kx){ 
		for(int wy=0; wy< output_height;wy++){
			for(int wx=0; wx< output_width;wx++) {
				float dot_product = 0.0f;
				for(int icx = 0; icx < input_channels; ++icx){
					input_offset = icx * (input_height + 2) * (input_width + 2);
					weight_offset = kx * input_channels * kernel_height * kernel_width 
								+ icx * kernel_height * kernel_width ;
					for(int ey=0;ey<kernel_height;ey++) {
						for(int ex=0;ex<kernel_width;ex++) {
							x = wx + ex;
							y = wy + ey;
							input_idx = y * (input_width +2) + x;
							weight_idx = ey * kernel_width + ex;

							dot_product += padded_input[ input_offset + input_idx ] 
								* weights[ weight_offset + weight_idx ];
						}
					}
				}
				output[ kx * output_height * output_width + wy * output_width + wx  ] = dot_product;
			}
		}
				
	}



}
