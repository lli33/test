

//data layout, from outputmost to innermost
// image: #in_channels x height x width
// kernel: #out_channels x in_channels x height x width
// output: #out_channels x output_height x output_width

#define Input_channels 128
#define Original_input_width 112
#define Original_input_height 112
#define Padded_Input_width 116
#define Padded_Input_height_for_main_kernel 111
#define Padded_Input_height_for_correctness 114
#define Kernel_width 3
#define Kernel_height 3
#define Num_kernels 128

#define Output_width 114
#define Output_height 111
#define Output_width_for_correctness 112
#define Output_height_for_correctness 112

#define Tile_size_x 5
#define Tile_size_y 5
#define In_channel_per_share 64


//-------------------------------
	
// dim0 to dim2: (76, 37, 128)


#define KERNEL_DIM0 76
#define KERNEL_DIM1 37
#define KERNEL_DIM2 128

#define KERNEL_ITER_OUTPUT_FACTOR 2


