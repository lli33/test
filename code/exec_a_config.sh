#!/usr/bin/env bash

set -e
set -o pipefail

# Activate debug
# set -x

include_path=$1

g++ -I $include_path -I ./driver_code/ ./driver_code/main.cpp -lOpenCL
./a.out $include_path
rm ./a.out


# g++ -I $include_path -I ./driver_code/ ./driver_code/main.cpp -I/home/shunya/armnn-onnx/ComputeLibrary/include -L/usr/lib/aarch64-linux-gnu/ -lmali -std=c++11 
# sudo ./a.out $include_path
