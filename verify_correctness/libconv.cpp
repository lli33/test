
#include <bits/stdc++.h>

using namespace std;

#include "./kernel_constants.h"

    

#include <iostream>
#include <CL/cl2.hpp>
#include <fstream>

std::string readFile(const char *filename){

  std::ifstream in(filename, std::ios::in);

  if (in.fail())
  {
  std::cerr << "Error reading file " << filename << std::endl;
  exit(1); }

  std::string contents;
  in.seekg(0, std::ios::end);
  contents.resize(in.tellg());
  in.seekg(0, std::ios::beg);
  in.read(&contents[0], contents.size());
  in.close();
  return contents;
  }

    

int platformId = 0;
int deviceId = 0;

 std::vector<cl::Platform> allPlatforms;
 cl::Platform platform;
 std::vector<cl::Device> allDevices;
 cl::Device device;
 cl::Context context;
 cl::CommandQueue lift_queue;

 size_t lift_global_0 = 1, lift_global_1 = 1, lift_global_2 =1;

      ; 
std::string kernel_string_431;
cl::Program::Sources kernel_source_431;
cl::Program kernel_program_431;
cl::Kernel kernel_431;
cl::Kernel kernel_final_reduction;
cl::Kernel kernel_padding;
cl::Kernel kernel_padding1;
cl::Kernel kernel_padding2;
cl::Kernel kernel_padding3;
cl::Kernel kernel_last_row;
std::chrono::milliseconds cpu_clock_start_431;
std::chrono::milliseconds cpu_clock_end_431;
std::chrono::milliseconds cpu_clock_start_padding;
std::chrono::milliseconds cpu_clock_end_padding;
cl::Event event_431;
cl::Event event_final_reduction;
cl::Event event_padding;
cl::Event event_padding1;
cl::Event event_padding2;
cl::Event event_padding3;
cl::Event event_last_row;
; 
; 
; 
; 
; 
; 
; 
; 
; 
; 
; 
; 
void lift_init(const std::string & pwd){
    
	cl::Platform::get(&allPlatforms);
 if (allPlatforms.size() == 0) {
 std::cerr << " No platforms found. Check OpenCL installation!" << std::endl;
 exit(1);
 }

 platform = allPlatforms[platformId];

 platform.getDevices(CL_DEVICE_TYPE_ALL, &allDevices);
 if (allDevices.size() == 0) {
 std::cerr << " No devices found. Check OpenCL installation!" << std::endl;
 exit(1);
 }

 device = allDevices[deviceId];

 std::cerr << "Using platform: " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;
 std::cerr << "Using device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;

 // create context
 cl::Context tmp_context({ device });
 context = std::move(tmp_context);

 // create queue
 cl::CommandQueue tmp_queue(context, device, CL_QUEUE_PROFILING_ENABLE);
 lift_queue = std::move(tmp_queue);
      ; 
    kernel_string_431 = readFile((pwd + "/kernel_431.cl").c_str()); 
    kernel_source_431 = cl::Program::Sources(1, {kernel_string_431.c_str(), kernel_string_431.length()}); 
    kernel_program_431 = cl::Program(context, kernel_source_431); 


    //not too much use
    /* const char *options = "-fkernel-vectorizer=4 -fkernel-unroller=2"; */
    /* if ((kernel_program_431.build({ device }, options, NULL, NULL) != CL_SUCCESS)){ */

    if ((kernel_program_431.build({ device }) != CL_SUCCESS)){
        std::cerr << "kernel build error" << std::endl; 
        char* log; 
        size_t logsize; 
        assert((clGetProgramBuildInfo(kernel_program_431.get(), device.get(), CL_PROGRAM_BUILD_LOG, 0, NULL, &logsize) == CL_SUCCESS)); 
        log = (char*)malloc(sizeof(char) * logsize); 
        assert((clGetProgramBuildInfo(kernel_program_431.get(), device.get(), CL_PROGRAM_BUILD_LOG, logsize, log, NULL) == CL_SUCCESS)); 
        std::cout << log << std::endl; 
        exit(1); 
    }
    kernel_431 = cl::Kernel(kernel_program_431, "KERNEL"); 
    kernel_final_reduction = cl::Kernel(kernel_program_431, "REDUCTION"); 
    //kernel_padding = cl::Kernel(kernel_program_431, "PADDING"); 
    kernel_padding1 = cl::Kernel(kernel_program_431, "PADDING_1"); 
    kernel_padding2 = cl::Kernel(kernel_program_431, "PADDING_2"); 
    kernel_padding3 = cl::Kernel(kernel_program_431, "PADDING_3"); 
    kernel_last_row = cl::Kernel(kernel_program_431, "LAST_ROW_KERNEL"); 

}

double cpu_time_in_ms( std::chrono::milliseconds start, std::chrono::milliseconds finish ){
 return (finish - start).count();
}

double gpu_time_in_ms( cl::Event event ){

  cl_ulong start;
  cl_ulong end;
  cl_int err;

  event.wait();

  err = clGetEventProfilingInfo(event(), CL_PROFILING_COMMAND_START,
                                sizeof(start), &start, NULL);
  assert(err == CL_SUCCESS);

  err = clGetEventProfilingInfo(event(), CL_PROFILING_COMMAND_END,
                                sizeof(end), &end, NULL);
  assert(err == CL_SUCCESS);

  return ((double)(end - start)) * 1.0e-6;
}

double diff_percent(double lhs, double rhs) {
  if(std::min(lhs,rhs)==0)
    return -1;
  else
    return std::abs(lhs-rhs)/std::min(lhs,rhs);
}

          ; 
void print_clock(){
    std::cerr<<"func_name, cpu_time_ms, gpu_time_ms, diff_percentage"<<std::endl; 
    {

        double cpu_time_ms0 = cpu_time_in_ms(cpu_clock_start_padding, cpu_clock_end_padding);
        std::cout<<"Padding"<<", "<< cpu_time_ms0 <<", "<<"NA"<<", "<<"NA"<<std::endl; 
        double gpu_time_ms0_1 = gpu_time_in_ms(event_padding1);
        double gpu_time_ms0_2 = gpu_time_in_ms(event_padding2);
        double gpu_time_ms0_3 = gpu_time_in_ms(event_padding3);
	std::cout << "Padding1" << "," << "NA" << "," << gpu_time_ms0_1 << ", NA" << std::endl;
	std::cout << "Padding2" << "," << "NA" << "," << gpu_time_ms0_2 << ", NA" << std::endl;
	std::cout << "Padding3" << "," << "NA" << "," << gpu_time_ms0_3 << ", NA" << std::endl;

	
        double cpu_time_ms = cpu_time_in_ms(cpu_clock_start_431, cpu_clock_end_431);
        double gpu_time_ms = gpu_time_in_ms(event_431);
        double diff_pc = diff_percent(cpu_time_ms, gpu_time_ms);
        double gpu_time_ms_last = gpu_time_in_ms(event_last_row);
        std::cout<<"OclFunCall_431"<<", "<<cpu_time_ms<<", "<<gpu_time_ms<<", "<<diff_pc<<std::endl; 
        std::cout<<"OclFunCall_last_row"<<", "<<cpu_time_ms<<", "<<gpu_time_ms_last<<", "<<diff_pc<<std::endl; 

        double gpu_time_ms2 = gpu_time_in_ms(event_final_reduction);
        std::cout<<"Final_reduction"<<", "<< "NA"<<", "<<gpu_time_ms2<<", "<<"NA"<<std::endl; 

	std::cout << std::endl;
	std::cout << "total_using_cpu_timer_for_padding = " << cpu_time_ms << std::endl;
	//std::cout << "total_using_gpu_timer_for_padding = " << gpu_time_ms0_1 + gpu_time_ms0_2 + gpu_time_ms0_3 + gpu_time_ms + gpu_time_ms2 << std::endl;
	std::cout << "total_using_gpu_timer_for_padding = " << gpu_time_ms0_1 + gpu_time_ms0_3 + gpu_time_ms0_2 + gpu_time_ms + gpu_time_ms_last + gpu_time_ms2 << std::endl;
	
    }
}
void post_execute(){
    print_clock(); 
}

namespace lift {; 

void execute(
		float * v_initial_param_419_63, 
		float * v_initial_param_420_64, 
		float * & v_user_func_432_70, 
		const int input_size, 
		const int padded_input_size , 
		const int weight_size , 
		const int tmp_output_length ,
		const int final_output_length
		){

    cl::Buffer v_user_func_426_66(context, CL_MEM_READ_ONLY , (input_size * sizeof(float)));
    cl::Buffer padded_input(context, CL_MEM_READ_WRITE , (padded_input_size * sizeof(float)));
    cl::Buffer v_user_func_428_67(context, CL_MEM_READ_ONLY , (weight_size * sizeof(float)));

    cl::Buffer v_user_func_431_69(context, CL_MEM_READ_WRITE , (KERNEL_ITER_OUTPUT_FACTOR*tmp_output_length * sizeof(float)));
    cl::Buffer final_output(context, CL_MEM_WRITE_ONLY , (final_output_length * sizeof(float)));
    v_user_func_432_70 = reinterpret_cast<float *>(malloc((final_output_length * sizeof(float)))); 
    

    lift_queue.enqueueWriteBuffer(v_user_func_426_66, CL_TRUE, 0, (input_size * sizeof(float)), v_initial_param_419_63, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    ; 
    lift_queue.enqueueWriteBuffer(v_user_func_428_67, CL_TRUE, 0, (weight_size * sizeof(float)), v_initial_param_420_64, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS);  
    ; 
    ; 
    kernel_padding2.setArg(0, v_user_func_426_66);
    kernel_padding2.setArg(1, padded_input);
    kernel_padding1.setArg(0, padded_input);
    kernel_padding3.setArg(0, padded_input);

    kernel_431.setArg(0, padded_input); 
    kernel_431.setArg(1, v_user_func_428_67); 
    kernel_431.setArg(2, v_user_func_431_69); 
    kernel_last_row.setArg(0, padded_input); 
    kernel_last_row.setArg(1, v_user_func_428_67); 
    kernel_last_row.setArg(2, v_user_func_431_69); 


    kernel_final_reduction.setArg(0, v_user_func_431_69); 
    kernel_final_reduction.setArg(1, final_output); 







    lift_queue.enqueueNDRangeKernel(kernel_padding2, cl::NullRange, cl::NDRange(Original_input_width/4,Original_input_height,Input_channels), cl::NullRange, NULL, NULL); 
    lift_queue.enqueueNDRangeKernel(kernel_padding1, cl::NullRange, cl::NDRange(Padded_Input_width/4,Input_channels,1), cl::NullRange, NULL, NULL); 
    lift_queue.enqueueNDRangeKernel(kernel_padding3, cl::NullRange, cl::NDRange(Padded_Input_width/4,1,Input_channels), cl::NullRange, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ;
    lift_queue.enqueueNDRangeKernel(kernel_431, cl::NullRange, cl::NDRange(KERNEL_DIM0, KERNEL_DIM1, KERNEL_DIM2), cl::NullRange, NULL, NULL); 
    // 2 here is how many output each thread generate
    lift_queue.enqueueNDRangeKernel(kernel_last_row, cl::NullRange, cl::NDRange( ( Output_width_for_correctness / 2 ) * KERNEL_ITER_OUTPUT_FACTOR,1,Num_kernels), cl::NullRange, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 

    //final reduction
    lift_queue.enqueueNDRangeKernel(kernel_final_reduction, cl::NullRange, cl::NDRange(Output_width_for_correctness,Output_height_for_correctness,Num_kernels), cl::NullRange, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    


    
    
    cpu_clock_start_431 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 

    lift_queue.enqueueNDRangeKernel(kernel_padding2, cl::NullRange, cl::NDRange(Original_input_width/4,Original_input_height,Input_channels), cl::NullRange, NULL, &event_padding2); 
    lift_queue.enqueueNDRangeKernel(kernel_padding1, cl::NullRange, cl::NDRange(Padded_Input_width/4,Input_channels,1), cl::NullRange, NULL, &event_padding1); 
    lift_queue.enqueueNDRangeKernel(kernel_padding3, cl::NullRange, cl::NDRange(Padded_Input_width/4,1,Input_channels), cl::NullRange, NULL, &event_padding3); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ;
    lift_queue.enqueueNDRangeKernel(kernel_431, cl::NullRange, cl::NDRange(KERNEL_DIM0, KERNEL_DIM1, KERNEL_DIM2), cl::NullRange, NULL, &event_431); 
    lift_queue.enqueueNDRangeKernel(kernel_last_row, cl::NullRange, cl::NDRange(( Output_width_for_correctness / 2 ) * KERNEL_ITER_OUTPUT_FACTOR,1,Num_kernels), cl::NullRange, NULL, &event_last_row); 
    assert(lift_queue.finish() == CL_SUCCESS); 

    //final reduction
    lift_queue.enqueueNDRangeKernel(kernel_final_reduction, cl::NullRange, cl::NDRange(Output_width_for_correctness,Output_height_for_correctness,Num_kernels), cl::NullRange, NULL, &event_final_reduction); 
    assert(lift_queue.finish() == CL_SUCCESS); 

    cpu_clock_end_431 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 

   
    ; 
    
   post_execute(); 
   

    lift_queue.enqueueReadBuffer(final_output, CL_TRUE, 0, (final_output_length * sizeof(float)), v_user_func_432_70, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 


}
}; 
