#include <bits/stdc++.h>

using namespace std;

#include <kernel_constants.h>


#include "./cpu.cpp"




#ifdef TEST
#include "./libconv.cpp"
extern void execute(float const *input, float const *weights, float const *biases, float * output);
#endif

void read_array_from_file(string const& in_file, vector<float> & res ) {

	ifstream Source(in_file.c_str());
	istream_iterator<float> Pos(Source), End;

	if(Pos == End)
		cout << "File not found!" << endl;
	else
	{
		//int const size = *Pos;
		//res = new vector<float>(size, 0.0f);
		copy(Pos, End, res.begin());
		/* copy(res.begin(), res.end(), ostream_iterator<float>(cout, " ")); */
		/* std::cout << std::endl; */
	}
}

int main(int argc, char *argv[])
{

#ifdef TEST
	assert(argc == 2);
	const std::string kernel_path(argv[1]);
#endif


	constexpr int input_channels = Input_channels;
	constexpr int input_height = Original_input_height;
	constexpr int input_width = Original_input_width;
	constexpr int padded_input_height = Padded_Input_height_for_correctness;
	constexpr int padded_input_width = Padded_Input_width;
	constexpr int kernel_height = Kernel_height;
	constexpr int kernel_width = Kernel_width;
	constexpr int num_kernels = Num_kernels;
	constexpr int output_height = Output_height_for_correctness;
	constexpr int output_width = Output_width_for_correctness;
	constexpr int tmp_output_width = Output_width;
	constexpr int tmp_output_height = Output_height_for_correctness;

	constexpr int kernel_stride_height = 1;
	constexpr int kernel_stride_width = 1;

	constexpr int input_length = input_channels * input_height * input_width;
	constexpr int padded_input_length = input_channels * padded_input_height * padded_input_width;
	constexpr int kernel_length = num_kernels * input_channels * kernel_height * kernel_width ;
	constexpr int output_length = num_kernels * output_height * output_width ;
	constexpr int tmp_output_length = num_kernels * tmp_output_height * tmp_output_width ; 

	vector<float> input(input_length,0), kernel(kernel_length,0);

#ifndef TEST
	std::srand(std::time(nullptr));
	generate(input.begin(), input.end(), [](){ return (rand()%100000)/100000.0f;} );

	std::ofstream input_file("./input_cpu.txt");
	copy(input.begin(), input.end(),   ostream_iterator<float>(input_file, " "));
	assert(input.size() == input_length);

	generate(kernel.begin(), kernel.end(), [](){ return (rand()%100000)/100000.0f;} );

	std::ofstream weight_file("./weights.txt");
	copy(kernel.begin(), kernel.end(),   ostream_iterator<float>(weight_file, " "));

	vector<float> output(output_length, 0);



#else
	//read input, kernel, biases
	read_array_from_file("./input_cpu.txt", input);
	assert(input.size() == input_length);
	read_array_from_file("./weights.txt", kernel);
	assert(kernel.size() == kernel_length);

	float * output = nullptr;

	float first_sample = 0;
	for (int ic = 0; ic < input_channels; ++ic) {

		for (int i = 0; i < kernel_height - 1; ++i) {
			for (int j = 0;  j < kernel_width - 1; ++j) {
				first_sample += input[ ic*input_height*input_width + i*input_width + j] 
					* kernel[ ic*kernel_height*kernel_width +  (i+1)*kernel_width + j + 1  ] ;
			}
		}

	}
	std::cout << "first element should be: " << first_sample << std::endl;

#endif



#ifndef TEST
	std::chrono::milliseconds cpu_clock_start;
	std::chrono::milliseconds cpu_clock_end;

	cpu_clock_start = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
	golden_convolution(input.data(), kernel.data(), output.data(), input_channels, input_height, input_width, kernel_height, kernel_width, num_kernels);
	cpu_clock_end = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
	std::cout << "Time = " << (cpu_clock_end - cpu_clock_start).count() << "ms" <<  std::endl;

	std::ofstream output_file("./output_cpu.txt");
	copy( output.begin(), output.end(), std::ostream_iterator<float>(output_file, " ") );

#else

	std::cout << tmp_output_length << std::endl;
	lift_init(kernel_path);
	lift::execute(input.data(), kernel.data(), output, input_length, padded_input_length, kernel_length, tmp_output_length, output_length);

	std::ofstream output_file("./output_gpu.txt");
	copy( output, output + output_length, std::ostream_iterator<float>(output_file, " ") );

	std::ifstream output_cpu("./output_cpu.txt");
	vector<float> output_golden(output_length, 0);
	read_array_from_file("./output_cpu.txt", output_golden);
	assert(output_golden.size() == output_length); 


	
	/*
	for (int i = 0; i < output_length; ++i) {
		output[i] += 
			output[i + output_length] + output[i + 2*output_length] + output[i + 3*output_length]
	                + output[i + 4*output_length] + output[i + 5*output_length] + output[i + 6*output_length] + output[i + 7*output_length];
	} */

	int err_counter = 0;
	for (int i = 0; i < output_length; ++i) {

		if ( abs(output[i] - output_golden[i]) > 0.01f ) {
			cerr << "******************************************" << endl;
			cerr << "GPU compute wrong result at index: " << i << endl;
			cerr << std::setprecision(9) << "golden: " << output_golden[i] << endl;
			cerr << std::setprecision(9) << "computed: " << output[i] << endl;
			cerr << "******************************************" << endl;
			if( err_counter++ > 5)
				return 1;
		}
		
	}

#endif



	return 0;
}
